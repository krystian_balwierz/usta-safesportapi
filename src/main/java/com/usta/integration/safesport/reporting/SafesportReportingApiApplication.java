package com.usta.integration.safesport.reporting;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
public class SafesportReportingApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(SafesportReportingApiApplication.class, args);
	}
}
