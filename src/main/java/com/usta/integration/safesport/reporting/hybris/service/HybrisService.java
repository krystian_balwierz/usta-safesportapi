package com.usta.integration.safesport.reporting.hybris.service;

import com.usta.integration.safesport.reporting.api.model.ReportData;
import com.usta.integration.safesport.reporting.hybris.model.HybrisTokenInfo;

public interface HybrisService {

    HybrisTokenInfo getHybrisToken(String email);

    void updateSafeSportData(String accessToken, ReportData reportData);
}
