package com.usta.integration.safesport.reporting.api.model;


import lombok.*;

import java.io.Serializable;
import java.util.UUID;

@Getter
@Setter
@Builder
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class ReportData implements Serializable {
    //Unique identifier for the transaction.
    private Integer transactionID;

    //User’s first name.
    private String firstName;

    //User’s last name.
    private String lastName;

    //User’s email.
    private String email;

    //Different organizations use varying names for this, but it is typically the  “Membership
    // Number”. This field is only provided if your organization is using membership verification.
    private String memberIdentifier;

    //This is the URL of the activity and serves as a unique identifier.
    private String activityID;

    //The title of the activity being completed.
    private String activityTitle;

    //Uniquely identifies a user’s instance of an activity.
    private String registration;

    // Will follow this format: 2018-01-01T00:00:00
    private String dateStarted;

    //How far the user has progressed (0 to 100).
    private Integer progress;

    //Will follow this format: 2018-01-01T00:00:00
    private String dateCompleted;

    //Uniquely identifies a user’s completion of an activity.
    private UUID completionCode;
}
