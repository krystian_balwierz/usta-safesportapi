package com.usta.integration.safesport.reporting.api.service;

import com.usta.integration.safesport.reporting.api.model.ReportData;
import com.usta.integration.safesport.reporting.hybris.connector.HybrisConnector;
import com.usta.integration.safesport.reporting.hybris.model.HybrisTokenInfo;
import com.usta.integration.safesport.reporting.hybris.model.HybrisTokenResponse;
import com.usta.integration.safesport.reporting.hybris.service.HybrisService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ReportsServiceImpl implements ReportsService {

    @Autowired
    HybrisService hybrisService;

    @Override
    public void updateHybrisData(List<ReportData> reportDataList) {
        for (ReportData reportData : reportDataList) {
            HybrisTokenInfo token = hybrisService.getHybrisToken(reportData.getEmail());
            hybrisService.updateSafeSportData(token.getAccessToken(), reportData);
        }
    }
}
