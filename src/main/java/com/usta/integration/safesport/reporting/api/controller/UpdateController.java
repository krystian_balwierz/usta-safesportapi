package com.usta.integration.safesport.reporting.api.controller;

import com.usta.integration.safesport.reporting.api.model.ReportData;
import com.usta.integration.safesport.reporting.api.service.ReportsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("update")
@Secured("ROLE_UPDATE")
public class UpdateController {

    @Autowired
    private ReportsService reportsService;

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    public void update(@RequestBody List<ReportData> reportData) {
        reportsService.updateHybrisData(reportData);
    }
}
