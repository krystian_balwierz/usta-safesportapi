package com.usta.integration.safesport.reporting.hybris.connector;

import com.usta.integration.safesport.reporting.hybris.model.HybrisTokenResponse;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Component
public class HybrisConnectorImpl implements HybrisConnector {

    @Autowired
    @Qualifier("hybrisRestTemplate")
    RestTemplate hybrisRestTemplate;
    @Value("${hybris.auth-token.url}")
    String getTokenUrl;
    @Value("${hybris.url}")
    private String hybrisUrl;
    @Value("${hybris.retriveCustomerDetails}")
    private String retriveCustomerDetails;
    @Value("${hybris.updateCustomerDetailsUrl}")
    private String updateCustomerDetailsUrl;

    @Override
    public HybrisTokenResponse getHybrisToken(String email) {
        String url = hybrisUrl + getTokenUrl;
        ResponseEntity<HybrisTokenResponse> entity = hybrisRestTemplate.postForEntity(url, null, HybrisTokenResponse.class, email);
        return entity.getBody();
    }

    @Override
    public JSONObject getCustomerDetails(String accessToken, String email) {
        String url = hybrisUrl + retriveCustomerDetails + "?emailId=" + email;

        HttpEntity requestEntity = getHttpEntity(accessToken, null);

        ResponseEntity<String> entity = hybrisRestTemplate.exchange(url, HttpMethod.GET, requestEntity , String.class);
        return new JSONObject(entity.getBody());
    }

    @Override
    public void updateCustomerDetails(String accessToken, JSONObject userDetails) {
        String url = hybrisUrl + updateCustomerDetailsUrl;

        HttpEntity requestEntity = getHttpEntity(accessToken, userDetails.toString(2));

        hybrisRestTemplate.exchange(url, HttpMethod.POST, requestEntity , JSONObject.class);
    }

    private HttpEntity getHttpEntity(String accessToken, Object body) {
        HttpHeaders requestHeaders = new HttpHeaders();
        requestHeaders.add("Cookie", "OAuthToken=" + accessToken);
        requestHeaders.add("Content-Type", "application/json");
        return new HttpEntity(body, requestHeaders);
    }

}
