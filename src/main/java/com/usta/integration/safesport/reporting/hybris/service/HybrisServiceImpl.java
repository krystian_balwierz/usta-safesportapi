package com.usta.integration.safesport.reporting.hybris.service;

import com.usta.integration.safesport.reporting.api.model.ReportData;
import com.usta.integration.safesport.reporting.hybris.connector.HybrisConnector;
import com.usta.integration.safesport.reporting.hybris.model.HybrisTokenInfo;
import com.usta.integration.safesport.reporting.hybris.model.HybrisTokenResponse;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class HybrisServiceImpl implements HybrisService {
    @Autowired
    private HybrisConnector hybrisConnector;

    @Override
    public HybrisTokenInfo getHybrisToken(String email) {
        HybrisTokenResponse response = hybrisConnector.getHybrisToken(email);
        return new HybrisTokenInfo(response);
    }

    @Override
    public void updateSafeSportData(String accessToken, ReportData reportData) {
        JSONObject customerDetails = hybrisConnector.getCustomerDetails(accessToken, reportData.getEmail());
        updateSafeSportFields(customerDetails, reportData);
        removeUneccesaryFields(customerDetails);
        hybrisConnector.updateCustomerDetails(accessToken, customerDetails);
    }

    private void removeUneccesaryFields(JSONObject customerDetails) {
        customerDetails.remove("addresses");
    }

    private void updateSafeSportFields(JSONObject customerDetails, ReportData reportData) {
        if (reportData.getProgress().equals(100)) {
            updateCompleted(customerDetails, reportData);
        } else {
            updateInProgress(customerDetails, reportData);
        }
    }

    private void updateInProgress(JSONObject customerDetails, ReportData reportData) {
        updateJSONField(customerDetails, "safeSportLaunchCourse", "true");
        updateJSONField(customerDetails, "safeSportStatus", "NOTYETCOMPLETE");
        if (reportData.getDateStarted() != null) {
            updateJSONField(customerDetails, "safeSportLaunchDateTime", reportData.getDateStarted());
        }
        updateJSONField(customerDetails, "safeSportRegID", reportData.getRegistration());
    }

    private void updateCompleted(JSONObject customerDetails, ReportData reportData) {
        updateJSONField(customerDetails, "safeSportLaunchCourse", "true");
        updateJSONField(customerDetails, "safeSportStatus", "COMPLETE");
        updateJSONField(customerDetails, "safeSportLaunchDateTime", reportData.getDateStarted());
        updateJSONField(customerDetails, "safeSportCompleteDateTime", reportData.getDateCompleted());
        updateJSONField(customerDetails, "safeSportRegID", reportData.getRegistration());
    }

    private void updateJSONField(JSONObject jsonObject, String key, String value) {
        if (jsonObject.has(key)) {
            jsonObject.remove(key);
        }
        jsonObject.put(key, value);
    }
}
