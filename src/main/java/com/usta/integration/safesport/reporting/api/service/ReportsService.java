package com.usta.integration.safesport.reporting.api.service;

import com.usta.integration.safesport.reporting.api.model.ReportData;

import java.util.List;
import java.util.Optional;

public interface ReportsService {
    void updateHybrisData(List<ReportData> reportDataList);

}
