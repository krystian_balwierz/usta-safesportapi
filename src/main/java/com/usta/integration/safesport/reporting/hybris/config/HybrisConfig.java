package com.usta.integration.safesport.reporting.hybris.config;

import com.usta.integration.safesport.reporting.hybris.interceptor.LogRestTemplateInterceptor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

@Configuration
public class HybrisConfig extends AbstractRestTemplateConfig {

    @Bean(name = "hybrisRestTemplate")
    public RestTemplate getHybrisRestTemplate() {
        RestTemplate restTemplate = new RestTemplate(getClientHttpRequestFactory());
        restTemplate.getInterceptors().add(new LogRestTemplateInterceptor());
        return restTemplate;
    }

}
