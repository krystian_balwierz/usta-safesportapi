package com.usta.integration.safesport.reporting.hybris.connector;

import com.usta.integration.safesport.reporting.hybris.model.HybrisTokenResponse;
import org.json.JSONObject;

public interface HybrisConnector {
    HybrisTokenResponse getHybrisToken(String email);

    JSONObject getCustomerDetails(String accessToken, String email);

    void updateCustomerDetails(String accessToken, JSONObject userDetails);
}
