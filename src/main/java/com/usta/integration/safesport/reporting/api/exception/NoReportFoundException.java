package com.usta.integration.safesport.reporting.api.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class NoReportFoundException extends RuntimeException {
    public NoReportFoundException() {
        super();
    }

    public NoReportFoundException(String message, Throwable cause) {
        super(message, cause);
    }

    public NoReportFoundException(String message) {
        super(message);
    }

    public NoReportFoundException(Throwable cause) {
        super(cause);
    }
}
